# David Dieulivol's README

## About Me :wave:

Hi! I'm David, a Senior Backend Engineer on the [Development Analytics group](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/development-analytics/) at [GitLab](https://about.gitlab.com/). I live in [Biel/Bienne, Switzerland](https://goo.gl/maps/ipKThxcPmtfgkbYQ6) (GMT+1).

* :flag_ch: French-Swiss, living in Switzerland for 20+ years
* :family_mwg: Husband/daddy
* :lifter_tone2: Crossfitter/Runner
* :book: Passionate about continuous learning
* :guitar: :drum: Music enthusiast

## Books I Recommend

* [Atomic Habits](https://www.amazon.com/Atomic-Habits-Proven-Build-Break/dp/0735211299) - Building better habits
* [Why we sleep](https://www.amazon.com/Why-We-Sleep-Unlocking-Dreams/dp/1501144316) - Understanding sleep science
* [Remote](https://www.amazon.com/Remote-Office-Required-Jason-Fried/dp/0804137501) & [Rework](https://www.amazon.com/Rework-Jason-Fried/dp/0307463745) - Modern work philosophy

## How I Work

### Daily Workflow
* Start with email/Slack triage
* Use [3 tasks system](https://www.youtube.com/watch?v=H5Sg3Gw8E0Y) for daily priorities
* Close communication tools while focusing on tasks
* Check messages roughly every hour

### Tools & Setup
* Task Management: [Trello](https://trello.com/en) with Personal Kanban system
* Editor: VSCode with GitLab integration
* Essential Apps: Time Out, Timer, Kap, Docker (Rancher Desktop)

### Development Environment
* VSCode with extensions:
  * GitLab integration
  * Markdown All in One
  * Project Manager
  * ShellCheck
* Chrome with separate work/personal profiles

## Working Style

* Strong preference for async communication
* Comfortable with both written and verbal interaction
* Thrive with multiple tasks and can handle ambiguity well
* Prefer verbal feedback followed by written documentation
* Value clear, written communication for tasks and expectations

## Team member profile

Comes from [this template](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/Team-Member-Profile.md), which itself comes from the [Autism Education Trust](https://www.autismeducationtrust.org.uk/).

> This template can be used by team members and managers to build a profile to enable more personalisation in the team member & manager relationship. This is particularly important for neurodiverse team members both diagnosed and undiagnosed, as they may have unique needs and accomodations that need to be made to ensure they are successful at GitLab. By focussing on styles rather than diagnosis it ensure that everyone has the opportunity to express individual needs without disclosing there neurodiversity if they do not wish too.

### Communication Style

- [x] I prefer to communicate verbally
- [x] I prefer to communicate through written communications 
- [ ] I do not communicate well in large groups
- [x] I can communicate well in large groups
- [ ] I can struggle with traditional social cues such as eye contact 
- [ ] I can struggle with talking about non-work related topics whilst at work
- [ ] Other: (Insert)

### Working Style

- [x] I benefit from work tasks being written or backed up with written communication
- [ ] I prefer visual information such as videos, charts, graphics and diagrams
- [ ] I prefer to record zoom meetings so I can review them later
- [x] I can handle multiple questions or instructions put to me at one time
- [ ] I prefer not to have multiple questions or instructions put to me at one time
- [ ] I tend to need time to process information before providing answers
- [ ] I prefer to have information or questions prior to meetings or discussions
- [x] I can struggle with deadlines, I appreciate more communication on tasks and deadlines
- [ ] I don't perform well with constant change, advanced notice to significant changes is preferred
- [ ] I work best when I can concentrate on a small number of tasks at one time
- [ ] Other: (Insert as many of your own statements as you see fit)

### Working Style Summary 

I prefer a work environment with lots of communication. I enjoy synchronous interactions, but I would lean heavily into asynchronous communication. I can deal with ambiguity well and thrive when I have multiple tasks that I am working on.

### Feedback

- [ ] I prefer verbal feedback
- [ ] I prefer written feedback
- [x] I prefer verbal feedback, followed up in writing
- [ ] I prefer written feedback, followed up with a verbal conversation

#### Are there any accommodations that can be made to help with you working environment 

No.
